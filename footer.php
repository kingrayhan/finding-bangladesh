    <!--Footer Start -->
    <footer>

        <div class="social-links">
            <ul class="social-link">
			<?php foreach (cs_get_option('theme_social_icons') as $social ): ?>
				<li><a href="<?php echo $social['social_link']; ?>">
					<i style="color:<?php echo $social['social_icon_color']; ?>" class="<?php echo $social['social_icon']; ?>"></i>
				</a></li>
            <?php endforeach; ?>
            </ul>
        </div>

        <div class="copy-right">
            <p>© Copyright 2018 Finding Bangladesh All Rights Reserved</p>
            <p>Developed By <a href="#">aniAds</a></p>
        </div>
    </footer>
    <!-- Footer End -->

    <?php wp_footer(); ?>
</body>

</html>
