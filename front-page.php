<?php  
$places = array( 
    'Dhaka',
    'Dhopodi Moth',
    'Gazi Kalu',
    'Boro Bazar Mosque',
    'Jorashib Temple',
    'Abhaynagor Temple',
    'Mohilara Moth',
    'Shyamnagor Rajbari',
    'Shonabaria Temple',
    'Shat Gombuj Mosque',
    'Kodhla Moth',
    'Krittipasha Rajbari',
    'Damrail Temple',
    'Rayerkathi Rajbari',
    'Ghaseti Begum Mosque',
    'Bon Bibi',
    'Shibsha Temple',
    'Misripara Buddha Vihar'
);


?>

<?php get_header(); ?>

    <div class="main-container">
        <!--Slog Section Start -->
        <div class="slog">
            <div class="head text-1">
                <h2>11 Stories</h2>
            </div>
            <div class="head text-2">
                <h2>17 historical Locations</h2>
            </div>
            <div class="head text-3">
                <h2>80 minutes</h2>
            </div>
        </div>
        <!-- Slog Section End-->

        <!-- Place Section Start-->
        <div class="places">
            <div class="site-overlay"></div>

            
    
            <?php foreach ($places as $index => $city):?>
                <div class="place-<?php echo ($index + 1); ?> phover placewidth">
                    


                    <a href="javascript:void(0)"> 
                        <?php if(cs_get_option('place_' . ($index + 1) . '_icon')): ?>
                        <img src="<?php echo cs_get_option('place_' . ($index + 1) . '_icon'); ?>" alt="<?php echo $city; ?>" class="placeimg"> 
                        <?php endif; ?>
                    </a>



                    <div class="placedetails place01des">
                        <h3><?php echo $city; ?></h3>
                        <p><?php echo cs_get_option('place_' . ($index + 1) . '_desc') ?></p>
                        <a href="<?php echo cs_get_option('place_' . ($index + 1) . '_url'); ?>" class="pleac-btn">Read More</a>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>

        <!-- Place Section End -->
    </div>

    <!-- Main Area End-->

<?php get_footer(); ?>