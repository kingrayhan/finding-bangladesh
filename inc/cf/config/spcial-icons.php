<?php
$options[]      = array(
	'name'        => 'social-icons',
	'title'       => 'Social Icons',
	'icon'        => 'fa fa-facebook',

	// begin: fields
	'fields'      => array(
		array(
		  'id'              => 'theme_social_icons',
		  'type'            => 'group',
		  'title'           => 'Social Icons',
		  'button_title'    => 'Add New Icons',
		  'accordion_title' => 'Add New Icons',
		  'fields'          => array(
		    
		    array(
		      'id'    => 'social_icon',
		      'type'  => 'icon',
		      'title' => 'Icon',
		    ),
		    array(
		      'id'    => 'social_icon_color',
		      'type'  => 'color_picker',
		      'title' => 'Icon color',
		      'default' => '#3b5999'
		    ),
		    array(
		      'id'    => 'social_link',
		      'type'  => 'text',
		      'title' => 'Url',
		    )


		  ),
		),
	)
);