<?php
$options[]      = array(
	'name'        => 'footer',
	'title'       => 'Footer',
	'icon'        => 'fa fa-map-marker',

	// begin: fields
	'fields'      => array(
		array(
			'id' => 'footer-copy-right',
			'type' => 'textarea',
			'title' => 'Footer copyright text',
			'default' => '<p>© Copyright 2018 Finding Bangladesh All Rights Reserved</p>
            <p>Developed By <a href="#">aniAds</a></p>'
		)
	)
);