<?php

$options[]      = array(
	'name'        => 'general',
	'title'       => 'General',
	'icon'        => 'fa fa-cube',

	// begin: fields
	'fields'      => array(
		array(
			'id' => 'site-logo',
			'type' => 'upload',
			'title' => 'Site logo',
			'desc' => 'upload image within size 200px ⤫ 200px',
			'default' => get_template_directory_uri() . '/assets/img/logo.png',
			'settings'      => array(
			   'upload_type'  => 'image',
			   'button_title' => 'Upload Logo',
			   'frame_title'  => 'Select a image for logo',
			   'insert_title' => 'Use as logo',
			  ),
		)
	)
);