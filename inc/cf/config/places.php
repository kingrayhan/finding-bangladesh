<?php

$places = array( 
	'Dhaka',
	'Dhopodi Moth',
	'Gazi Kalu',
	'Boro Bazar',
	'Jorashib Temple',
	'Abhaynagor Temple',
	'Mohilara Moth',
	'Shyamnagor Rajbari',
	'Shonabaria Temple',
	'Shat Gombuj Mosque',
	'Kodhla Moth',
	'Krittipasha Rajbari',
	'Damrail Temple',
	'Rayerkathi Rajbari',
	'Ghaseti Begum Mosque',
	'Bon Bibi',
	'Shibsha Temple',
	'Misripara Buddha Vihar'
);


$city_fields = array();

foreach ($places as $index => $city) {
	array_push($city_fields, 
		array(
		  'type'    => 'heading',
		  'content' => $city,
		)
	);
	array_push($city_fields, 
		array(
		  'id' => 'place_' . ($index + 1) . '_desc',
		  'type' => 'textarea',
		  'title' => 'Description',
		  'attributes'    => array(
		      'minlength' => 200,
		      'maxlength' => 300
		   )
		)
	);
	array_push($city_fields, 
		array(
		  'id' => 'place_' . ($index + 1) . '_url',
		  'type' => 'text',
		  'title' => 'Url'
		)
	);

	array_push($city_fields, 
		array(
		  'id' => 'place_' . ($index + 1) . '_icon',
		  'type' => 'upload',
		  'title' => 'Upload <b>' . $city . '</b> icon'
		)
	);
}



$options[]      = array(
  'name'        => 'places',
  'title'       => 'Places',
  'icon'        => 'fa fa-map-marker',

  // begin: fields
  'fields'      => $city_fields
);