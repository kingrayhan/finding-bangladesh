<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Finding_Bangladesh
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

	<?php wp_head(); ?>

	<body <?php body_class(); ?>>
		    <!-- Header Start  -->
		    <header>
		        <!-- Pushy Menu -->
		        <!-- <nav class="pushy pushy-left" data-focus="#first-link">
		            <div class="pushy-content">
		                <ul>
		                    <li class="pushy-link"><a href="#">Home</a></li>
		                    <li class="pushy-submenu">
		                        <button id="first-link">Watch Now</button>
		                        <ul>
		                            <li class="pushy-link"><a href="#">Music Videos</a></li>
		                            <li class="pushy-link"><a href="#">Episodes</a></li>
		                            <li class="pushy-link"><a href="#">trailer</a></li>
		                        </ul>
		                    </li>
		                    <li class="pushy-link"><a href="#">What is Finding Bangladesh</a></li>
		                    <li class="pushy-link"><a href="#">About The Team</a></li>
		                    <li class="pushy-link"><a href="#">Message From direcor</a></li>

		                    <li class="pushy-submenu">
		                        <button>Stories</button>
		                        <ul>
		                            <li class="pushy-link"><a href="#">Place 01</a></li>
		                            <li class="pushy-link"><a href="#">place 02</a></li>
		                            <li class="pushy-link"><a href="#">place 03</a></li>
		                        </ul>
		                    </li>
		                    <li class="pushy-link"><a href="#">Goods</a></li>
		                    <li class="pushy-link"><a href="#">Books</a></li>
		                    <li class="pushy-link"><a href="#">Contact Us</a></li>

		                </ul>
		            </div>
		        </nav> -->

		        <!-- <div class="site-overlay"></div> -->

<!-- 
		        <div id="container">
		            <button class="menu-btn">&#9776;</button>
		        </div> -->

		        <div class="site-logo">
		            <a href="<?php echo site_url(); ?>">
		            	<img src="<?php echo cs_get_option('site-logo'); ?>">
		            </a>
		        </div>

		    </header>

		    <!-- Header End  -->
